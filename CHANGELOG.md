# Changelog

We are currently working on porting this changelog to the specifications in
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Version 0.2.1 - Unreleased

### Added
* Add 3.11 support

### Changes
* Added i686 wheels

## Version 0.2.0 - Released 2022-06-21

### Fixed
* Python 3.10 wheels

## Version 0.1.0 - Released 2022-06-21

### Added
* Initial port from kwimage

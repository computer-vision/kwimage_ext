"""
Binary extensions for kwimage

+------------------+-----------------------------------------------------------+
| Read the docs    | https://kwimage_ext.readthedocs.io                        |
+------------------+-----------------------------------------------------------+
| Gitlab (main)    | https://gitlab.kitware.com/computer-vision/kwimage_ext    |
+------------------+-----------------------------------------------------------+
| Pypi             | https://pypi.org/project/kwimage_ext                      |
+------------------+-----------------------------------------------------------+

"""
__version__ = '0.2.1'

__author__ = 'Kitware Inc., Jon Crall'
__author_email__ = 'kitware@kitware.com, jon.crall@kitware.com'
__url__ = 'https://gitlab.kitware.com/computer-vision/kwimage_ext'

__mkinit__ = """
mkinit -m kwimage_ext
"""

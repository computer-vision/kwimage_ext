# Add requirements here, use the script for help (erotemic needs to publish this script)
# python ~/local/tools/supported_python_versions_pip.py pygments
# python ~/local/tools/supported_python_versions_pip.py torch


torch>=1.13.0    ; python_version < '4.0'  and python_version >= '3.11'    # Python 3.11+
torch>=1.11.0    ; python_version < '3.11' and python_version >= '3.10'    # Python 3.10
torch>=1.9.0     ; python_version < '3.10' and python_version >= '3.9'     # Python 3.9
torch>=1.6.0     ; python_version < '3.9'  and python_version >= '3.6'  # Python 3.6-3.8
